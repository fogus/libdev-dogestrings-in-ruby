# Dogestrings

Hardcore docstrings for Ruby.

<img src="https://raw.githubusercontent.com/fogus/dogestrings/master/docs/logo.jpg" height="300px" width="300px">

# License

Copyright 2015 Mike Fogus

Licensed under the LGPL v3.0. https://www.gnu.org/licenses/lgpl-3.0.en.html