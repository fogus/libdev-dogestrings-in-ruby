require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

#
# Classes
#
class Foo
  ___ "Ants!"
  def self.bar
    "blah"
  end

  ___ "Monkeys!"
  def baz
    "boop"
  end

  def qux x
    [x]
  end

  class << self
    ___ "Chins!"
    def frob x, y
      [x,y]
    end
  end
end

describe "Foo" do
  it "has the expected docstrings" do
    expect(Foo.doc_for(:bar)).to eq "Ants!"
    expect(Foo.doc_for(:baz)).to eq "Monkeys!"
    expect(Foo.doc_for(:qux)).to be nil
    Foo.describe #TODO
  end
end

# 
# Modules
#

module MFoo
  ___ "Ants!"
  def self.bar
    "blah"
  end

  ___ "Monkeys!"
  def baz
    "boop"
  end

  def qux x
    [x]
  end
end

describe "MFoo" do
  it "has the expected docstrings" do
    expect(MFoo.doc_for(:bar)).to eq "Ants!"
    expect(MFoo.doc_for(:baz)).to eq "Monkeys!"
    expect(MFoo.doc_for(:qux)).to be nil
    MFoo.describe # TODO
  end
end

# 
# Mix-ins
#

class MxFoo
  extend MFoo

  ___ "Blarb!"
  def zzz
    42
  end
end

describe "MxFoo" do
  it "has the expected docstrings" do
    expect(MxFoo.doc_for(:bar)).to be nil
#    expect(MxFoo.doc_for(:baz)).to eq "Monkeys!"
    expect(MxFoo.doc_for(:zzz)).to eq "Blarb!"
    expect(MxFoo.doc_for(:qux)).to be nil
    MxFoo.describe
  end
end

